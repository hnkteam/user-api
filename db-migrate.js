const NODE_MODULES_LOCATION = "/usr/share/app/node_modules";

process.env.NODE_ENV = "development";
for (const arg of process.argv) {
    if (arg.indexOf("--env=") === 0) {
        process.env.NODE_ENV = arg.split("=")[1];
        break;
    }
}

// @todo - move config to separate file
let config = {
    development: {
        user: 'user_api',
        password: 'user_api',
        host: 'user-api-pgsql',
        port: 5432,
        database: 'user_api',
    },
    test: {
        user: 'user_api_test',
        password: 'user_api_test',
        host: 'user-api-pgsql',
        port: 5432,
        database: 'user_api_test',
    },
};

if (false === config.hasOwnProperty(process.env.NODE_ENV)) {
    console.log(`Config for env ${process.env.NODE_ENV} does not exist`);
    process.exit(1);
}

let databaseConfig = config[process.env.NODE_ENV];
process.env.DB_MIGRATE_USER = databaseConfig.user;
process.env.DB_MIGRATE_PASSWORD = databaseConfig.password;
process.env.DB_MIGRATE_HOST = databaseConfig.host;
process.env.DB_MIGRATE_DATABASE = databaseConfig.database;
process.env.DB_MIGRATE_PORT = databaseConfig.port;

// @todo check lines below
process.argv[1] = `${NODE_MODULES_LOCATION}/db-migrate/bin/db-migrate`;
process.argv.splice(2, 0, '--config');
process.argv.splice(3, 0, '/usr/share/app/migrations/config.json');

const Migration = require(`${NODE_MODULES_LOCATION}/db-migrate/lib/migration.js`);
Migration.prototype.defaultSqlTemplate = () => { return "BEGIN;\n--empty migration\nCOMMIT;"; };

require(`${NODE_MODULES_LOCATION}/db-migrate/bin/db-migrate`);