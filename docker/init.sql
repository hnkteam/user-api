DROP DATABASE IF EXISTS user_api;
DROP DATABASE IF EXISTS user_api_test;
DROP USER IF EXISTS user_api;
DROP USER IF EXISTS user_api_test;

CREATE USER user_api WITH ENCRYPTED PASSWORD 'user_api';
CREATE USER user_api_test WITH ENCRYPTED PASSWORD 'user_api_test';

CREATE DATABASE user_api OWNER user_api;
\c user_api
GRANT ALL ON SCHEMA public TO user_api;

CREATE DATABASE user_api_test OWNER user_api_test;
\c user_api_test
GRANT ALL ON SCHEMA public TO user_api_test;

