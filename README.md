# user-api

Simple api example, using node.js and postgresql.

## Setup

Repository comes with docker containers. By default api will listen at port 8080, but this port will be bound to port 8081 on the host machine.

###Using ant and docker
Create docker containers, install dependencies, initialize database:
```angular2html
ant build
```

Run app:
```angular2html
ant app-run-develop
```

Run tests:
```angular2html
ant app-run-ci
```

###Manually

TODO :)

## Examples

1. Importing users from a file

request:
```youtrack
PUT /user?encoding=utf-8 HTTP/1.1
Host: localhost:8081
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
Cache-Control: no-cache

------WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name=""; filename="users_valid_encoding.csv"
Content-Type: text/csv


------WebKitFormBoundary7MA4YWxkTrZu0gW--
```
Parameter encoding is optional, by default iso-8859-15 is expected.

response:
```youtrack
HTTP/1.1 204 No Content
```

2. Listing users

request:
```
GET /user?page=1&amp;limit=2 HTTP/1.1
Host: localhost:8081
Cache-Control: no-cache
```
Parameters page and limit are optional.

response:
```
HTTP/1.1 200 Ok
[
    {
        "id": 1,
        "email": "abbey@example.com",
        "firstName": "Abbey",
        "lastName": "Johnston",
        "roleDescription": "Abbey's responsibilities:\n- main product\n- code reviews",
        "createdAt": "2018-06-27T01:23:12.000Z",
        "updatedAt": null,
        "teamId": 1,
        "team": {
            "id": 1,
            "name": "Developers",
            "createdAt": "2018-06-27T01:23:12.000Z",
            "updatedAt": null
        }
    },
    {
        "id": 2,
        "email": "ala@makota.pl",
        "firstName": "Caitlyn",
        "lastName": "",
        "roleDescription": "",
        "createdAt": "2018-06-27T01:23:12.000Z",
        "updatedAt": null,
        "teamId": 2,
        "team": {
            "id": 2,
            "name": "Design",
            "createdAt": "2018-06-27T01:23:12.000Z",
            "updatedAt": null
        }
    },
    ...
]
```

3. Listing teams:

request:
```
GET /team HTTP/1.1
Host: localhost:8081
Cache-Control: no-cache
```

response:
```
HTTP/1.1 200 Ok
[
    {
        "id": 1,
        "name": "Developers",
        "createdAt": "2018-06-27T01:23:12.000Z",
        "updatedAt": null,
        "users": [
            {
                "id": 3,
                "email": "ettie@example.com",
                "firstName": "Ettie",
                "lastName": "Hauck",
                "roleDescription": "",
                "createdAt": "2018-06-27T01:23:12.000Z",
                "updatedAt": null,
                "teamId": 1
            },
        ]
    },
    ...
]
```