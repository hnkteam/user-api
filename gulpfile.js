const exec = require('child_process').exec;
const gulp = require('gulp');
const meow = require('meow');
const mocha = require('gulp-mocha');
const path = require('path');
const runSequence = require('run-sequence');
const server = require('gulp-develop-server');

const cli = meow();

process.env.NODE_ENV = (cli.flags.hasOwnProperty("env")) ? cli.flags['env'] :  process.env.NODE_ENV || 'development';
process.env.NODE_CONFIG_DIR = path.resolve(path.join(__dirname, '../config'));
process.env.TZ = 'Europe/Warsaw';

const SOURCE_DIR = path.resolve(`${__dirname}/src`);
const RELEASE_DIR = path.resolve(`${__dirname}/release`);

function doCompileTypescript(done) {
    exec('node_modules/.bin/tsc', function(error, stdout, stderror) {
        if (stdout) {
            console.log(stdout);
        }
        if (error) {
            done(error);
            return;
        }
        done();
    });
}

function runApp(isFirstRun, done) {
    if (isFirstRun) {
        server.listen(
            { path: `${RELEASE_DIR}/main.js` },
            done
        );
    } else {
        server.kill('SIGTERM', () => {
            server.listen(
                { path: `${RELEASE_DIR}/main.js` },
                done
            );
        });
    }
}

function buildApp(done) {
    doCompileTypescript(() => {
        done();
    });
}

gulp.task('typescript', function (done) {
    doCompileTypescript(function (error) {
        if (error) {
            console.log(error);
            process.exit(1);
        }

        done();
    });
});

gulp.task('develop', function() {
    buildApp(() => {
        runApp(true, () => {
            gulp.watch(`${SOURCE_DIR}/**/*`, () => {
                buildApp(() => {
                    runApp(false, () => {});
                })
            })

        })
    });
});

gulp.task('test-unit', () => {
    if (process.env.NODE_ENV !== 'test') {
        console.error('You MUST run unit tests only in test env');
        process.exit(1);
    }

    if (cli.flags.hasOwnProperty('skipUnit')) {
        return;
    }

    let exitOnError = false;
    let isError = false;
    let testFile = '*';
    let bail = false;
    let forbidOnly = false;

    if (cli.flags['file']) {
        testFile = cli.flags['file'];
    }
    if (cli.flags.hasOwnProperty('exitOnError')) {
        exitOnError = true;
    }
    if (cli.flags.hasOwnProperty('bail')) {
        bail = true;
    }
    if (cli.flags.hasOwnProperty('forbidOnly')) {
        forbidOnly = true;
    }

    let mochaOptions = {
        exit: true,
        bail: bail,
        forbidOnly: forbidOnly,
    };

    return gulp
        .src(`${RELEASE_DIR}/tests/unit/**/${testFile}.js`, { read: false })
        .pipe(mocha(mochaOptions))
        .once("end", () => {
            if (true === exitOnError && true === isError) {
                process.exit(1);
            }
        })
        .once("error", function (error) {
            isError = true;
            this.emit("end");
        })

});


gulp.task('test-functional', () => {
    if (process.env.NODE_ENV !== 'test') {
        console.error('You MUST run functional tests only in test env');
        process.exit(1);
    }

    if (cli.flags.hasOwnProperty('skipFunctional')) {
        return;
    }

    let exitOnError = false;
    let isError = false;
    let testFile = '*';
    let bail = false;
    let forbidOnly = false;

    if (cli.flags['file']) {
        testFile = cli.flags['file'];
    }
    if (cli.flags.hasOwnProperty('exitOnError')) {
        exitOnError = true;
    }
    if (cli.flags.hasOwnProperty('bail')) {
        bail = true;
    }
    if (cli.flags.hasOwnProperty('forbidOnly')) {
        forbidOnly = true;
    }

    let mochaOptions = {
        exit: true,
        bail: bail,
        forbidOnly: forbidOnly,
    };
    return gulp
        .src(`${RELEASE_DIR}/tests/functional/**/${testFile}.js`, { read: false })
        .pipe(mocha(mochaOptions))
        .once("end", () => {
            if (true === exitOnError && true === isError) {
                process.exit(1);
            }
        })
        .once("error", function (error) {
            isError = true;
            this.emit("end");
        })

});

gulp.task('db-migrate', (done) => {
    exec(`node db-migrate.js --env=${process.env.NODE_ENV} up`, function(error, stdout, stderror) {
        if (stdout) {
            console.log(stdout);
        }
        if (error) {
            console.log('stderr: ', stderror);
            console.log(error);
            process.exit(1);
        }
        done();
    });
});

gulp.task('exit', () => {
    process.exit();
});

gulp.task('ci', (done) => {
    process.env.NODE_CONFIG_DIR = path.resolve(path.join(__dirname, 'config'));
    runSequence(
        'typescript',
        'db-migrate',
        'test-unit',
        'test-functional',
        'exit',
        done
    );
});