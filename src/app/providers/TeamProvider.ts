import Team from "../models/Team";
import User from "../models/User";

export default class TeamProvider {

    public static async getTeamsIndexedByName(teamNames: string[]): Promise<{ [teamName: string]: Team } | {}> {
        let teams = await Team.findAll({ where: { name: teamNames } });

        if (teams.length === 0) {
            return {};
        }

        let teamsIndexedByName = {};
        for (const team of teams) {
            teamsIndexedByName[team.name] = team;
        }

        return teamsIndexedByName;
    }

    public static async getTeamsCount(): Promise<number> {
        return await Team.count();
    }

    public static async getTeamsChunk(offset: number, limit: number): Promise<Team[]> {
        return await Team.findAll({
            offset: offset,
            limit: limit,
            order: ['id'],
            include: [User]
        });
    }


}