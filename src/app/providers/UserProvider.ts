import User from "../models/User";
import Team from "../models/Team";

export default class UserProvider {
    
    public static async getUsersByEmails(emails: string[]): Promise<{ [email: string]: User }> {
        let users = await User.findAll({ where: { email: emails } });

        if (users.length === 0) {
            return {};
        }

        let usersIndexedByName = {};
        for (const user of users) {
            usersIndexedByName[user.email] = user;
        }

        return usersIndexedByName;
    }

    public static async getUsersCount(): Promise<number> {
        return await User.count();
    }

    public static async getUsersChunk(offset: number, limit: number): Promise<User[]> {
        return await User.findAll({
            offset: offset,
            limit: limit,
            order: ['id'],
            include: [Team]
        });
    }
    
}