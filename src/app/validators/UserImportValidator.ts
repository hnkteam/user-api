import validator = require('validator');
import ApiError from "../errors/ApiError";
import UserImportInterface from "../entities/UserImportInterface";
import UserValidationError from "../errors/UserValidationError";

export default class UserImportValidator {

    private errors: ApiError[] = [];

    private valid: boolean = null;

    private importedEmails: string[] = [];

    constructor(private data: UserImportInterface[], private startItem: number = 1) {}

    public isValid(): boolean {
        if (null === this.valid) {
            this.validate();
        }

        return this.valid;
    }

    public getErrors(): ApiError[] {
        return this.errors;
    }

    private validate(): void {
        let currentLine = this.startItem;

        for (const user of this.data) {
            this.validateUser(user, currentLine++);
        }

        this.valid = this.errors.length === 0;
    }

    private validateUser(userData: UserImportInterface, item: number) {
        if (validator.isEmpty(userData.email)) {
            this.addValidationError(UserValidationError.USER_IMPORT_EMPTY_USER_EMAIL, item);
        } else {
            if (!validator.isEmail(userData.email)) {
                this.addValidationError(UserValidationError.USER_IMPORT_INVALID_USER_EMAIL, item);
            }

            if (this.importedEmails.indexOf(userData.email.toLocaleLowerCase()) !== -1) {
                this.addValidationError(UserValidationError.USER_IMPORT_DUPLICATED_USER_EMAIL, item);
            }

            this.importedEmails.push(userData.email.toLocaleLowerCase());
        }

        if (validator.isEmpty(userData.team)) {
            this.addValidationError(UserValidationError.USER_IMPORT_EMPTY_TEAM, item);
        }

        // @todo validate other columns if needed

    }

    private addValidationError(code: number, item: number): void {
        let error = new UserValidationError(code);
        error.setAdditionalData({ item: item });
        this.errors.push(error);
    }

}