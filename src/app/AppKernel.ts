import restify = require("restify");
import ApiError from "./errors/ApiError";
import LoggerInterface from "./utils/LoggerInterface";
import RestifyHelper from "./utils/RestifyHelper";
import Router from "./routes/Router";
import ServerError from "./errors/ServerError";
import LoggerFactory from "./utils/LoggerFactory";
import DBFactory from "./models/DBFactory";

export default class AppKernel {

    private logger: LoggerInterface;

    private server: restify.Server;

    private isServerInitialized: boolean = false;

    constructor(logger: LoggerInterface) {
        this.logger = logger;
    }

    public async run(
        options: restify.ServerOptions,
        listenArgs: any[]
    ): Promise<void> {

        try {
            this.beforeCreateServer();

            this.createServer(options);

            await this.beforeStartServer();

            this.startServer(listenArgs);
        } catch (error) {
            throw ServerError.createFromUnknownError(error);
        }
    }

    public getServer(): restify.Server {
        this.throwIfNotInitialized();
        return this.server;
    }

    private beforeCreateServer(): void {
        this.logger.debug("before create server");
    }

    private async beforeStartServer(): Promise<void> {
        this.logger.debug("before start server");

        DBFactory.initializeDB();

        this.initializePreRoutingHandlers();
        this.initializeRouting();
        this.initializePostRoutingHandlers();
    }

    private createServer(options: restify.ServerOptions): void {
        this.server = restify.createServer(options);
        this.isServerInitialized = true;

        this.logger.debug("server created");
    }

    private startServer(listenArgs: any[]): void {

        listenArgs.push(() => {
            console.log(`${this.server.name} listening at ${this.server.url}`);
        });

        try {
            this.server.listen(...listenArgs);
            this.logger.debug("server started");
        } catch (error) {
            console.log(error);
            this.logger.error(error);
            process.exit(1);
        }

    }

    private initializePreRoutingHandlers(): void {
        this.setRequestHandler(restify.queryParser());

        this.setUncaughtExceptionListener();
    }

    private initializeRouting(): void {
        (new Router()).register(this.getServer());
    }

    private initializePostRoutingHandlers(): void {
        // this.setAuditLogger();
    }

    private setRequestHandler(handler:restify.RequestHandler);
    private setRequestHandler(handler:restify.RequestHandler[]);
    private setRequestHandler():void {
        this.server.use(arguments[0]);
    }

    private setUncaughtExceptionListener(): void {
        this.server.on(
            "uncaughtException",
            (request, response, route, error) => {
                if (process.env.NODE_ENV === 'test') {
                    return;
                }

                let apiError = ApiError.createFromUnknownError(error);

                this.logger.error(apiError);

                RestifyHelper.sendErrorResponse(response, apiError);
            }
        );
    }

    private setAuditLogger(): void {
        this.server.on(
            "after",
            restify.auditLogger({ log: LoggerFactory.getInstance().getLogger("audit") })
        );
    }

    private throwIfNotInitialized(): void {
        if (!this.isServerInitialized) {
            throw new ServerError(ServerError.KERNEL_SERVER_NOT_INITIALIZED);
        }
    }

}



