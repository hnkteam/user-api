
interface UserImportInterface {
    firstName: string;
    lastName: string;
    email: string;
    roleDescription: string;
    team: string;
}

export default UserImportInterface;