import async = require("async");
import httpStatus = require("http-status");
import restify = require("restify");
import ApiError from "../errors/ApiError";
import CsvUploadHandler from "../utils/CsvUploadHandler";
import RestifyHelper from "../utils/RestifyHelper";
import RouteInterface from "./RouteInterface";
import UserController from "../controllers/UserController";
import UserImportInterface from "../entities/UserImportInterface";
import UserImportValidator from "../validators/UserImportValidator";
import UserValidationError from "../errors/UserValidationError";
import BadRequestError from "../errors/BadRequestError";
import PaginationBuilder from "../utils/PaginationBuilder";

export default class UserRoute implements RouteInterface {

    private userController: UserController;

    constructor() {
        this.userController = new UserController();
    }

    register(server: restify.Server): void {

        server.put(
            { path: "/user" },

            restify.bodyParser({
                // @todo - add typing and move to utils
                // @todo - handle max body size
                multipartFileHandler: (stream, request) => {
                    let inputEncoding = "iso-8859-15";

                    if (request.query.encoding) {
                        inputEncoding = request.query.encoding;
                    }

                    let uploader = new CsvUploadHandler<UserImportInterface>({
                        inputEncoding: inputEncoding,
                        outputEncoding: "utf-8",
                        parserOptions: {
                            columns: ["firstName", "lastName", "email", "roleDescription", "team"],
                            from: 2,
                            delimiter: ',',
                            skip_empty_lines: true,
                            trim: true,
                        }
                    });
                    request['uploader'] = uploader;
                    uploader.readStream(stream);

                }
            }),

            (request: restify.Request, response: restify.Response, next: restify.Next) => {

                const uploader = request['uploader'];

                if (!uploader) {
                    return RestifyHelper.sendErrorResponse(response, new BadRequestError(BadRequestError.MISSING_INPUT_FILE), next);
                }

                async.waterfall(
                    [
                        (next) => {
                            uploader.getParser().on("finish", () => {
                                let users = uploader.getData();

                                const validator = new UserImportValidator(users);

                                if (false === validator.isValid()) {
                                    return next(
                                        new UserValidationError(
                                            UserValidationError.USER_IMPORT_VALIDATION_ERROR
                                        ).setSubErrors(validator.getErrors())
                                    );
                                }

                                next(null, users);
                            });
                        },

                        (users: UserImportInterface[], next) => {
                            this.userController.importUsers(users)
                                .catch(next)
                                .then(next);
                        }
                    ],
                    (error: ApiError) => {
                        if (error) {
                            return RestifyHelper.sendErrorResponse(response, error, next);
                        }

                        RestifyHelper.sendResponse(response, httpStatus.NO_CONTENT, null, next);
                    }
                );
            }
        );

        server.get(
            { path: "/user"},

            (request: restify.Request, response: restify.Response, next: restify.Next) => {

                let pagination;
                try {
                    pagination = (new PaginationBuilder(1, 25, 100)).build(
                        request.query.page,
                        request.query.limit,
                    )
                } catch (error) {
                    return RestifyHelper.sendErrorResponse(response, ApiError.createFromUnknownError(error), next);
                }

                this.userController.getUsersList(pagination)
                    .catch((error) => {
                        return RestifyHelper.sendErrorResponse(response, error, next);
                    })
                    .then((users) => {
                        RestifyHelper.sendResponse(response, httpStatus.OK, users, next);
                    });
            }
        );

    }

}