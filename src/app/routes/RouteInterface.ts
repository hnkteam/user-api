import restify = require("restify");

interface RouteInterface {
    register(server: restify.Server): void;
}

export default RouteInterface;
