import httpStatus = require("http-status");
import restify = require("restify");
import ApiError from "../errors/ApiError";
import RestifyHelper from "../utils/RestifyHelper";
import RouteInterface from "./RouteInterface";
import PaginationBuilder from "../utils/PaginationBuilder";
import TeamController from "../controllers/TeamController";

export default class TeamRoute implements RouteInterface {

    private teamController: TeamController;

    constructor() {
        this.teamController = new TeamController();
    }

    register(server: restify.Server): void {

        server.get(
            { path: "/team"},

            (request: restify.Request, response: restify.Response, next: restify.Next) => {

                let pagination;
                try {
                    pagination = (new PaginationBuilder(1, 25, 100)).build(
                        request.query.page,
                        request.query.limit,
                    )
                } catch (error) {
                    return RestifyHelper.sendErrorResponse(response, ApiError.createFromUnknownError(error), next);
                }

                this.teamController.getTeamsList(pagination)
                    .catch((error) => {
                        return RestifyHelper.sendErrorResponse(response, error, next);
                    })
                    .then((users) => {
                        RestifyHelper.sendResponse(response, httpStatus.OK, users, next);
                    });
            }
        );


    }
}