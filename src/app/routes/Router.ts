import restify = require("restify");
import InfoRoute from "./InfoRoute";
import TeamRoute from "./TeamRoute";
import UserRoute from "./UserRoute";

export default class Router {

    register(server: restify.Server): void {
        (new InfoRoute()).register(server);
        (new UserRoute()).register(server);
        (new TeamRoute()).register(server);
    }

}