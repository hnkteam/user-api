import httpStatus = require("http-status");
import restify = require("restify");
import InfoController from "../controllers/InfoController";
import RestifyHelper from "../utils/RestifyHelper";
import RouteInterface from "./RouteInterface";

export default class InfoRoute implements RouteInterface {

    register(server: restify.Server): void {

        server.get(
            { path: "/info" },
            (request: restify.Request, response: restify.Response, next: restify.Next) => {

                (new InfoController()).info()
                    .catch(error => {
                        RestifyHelper.sendErrorResponse(response, error, next);
                    })
                    .then(data => {
                        RestifyHelper.sendResponse(response, httpStatus.OK, data, next);
                    });

            }
        )

    }

}