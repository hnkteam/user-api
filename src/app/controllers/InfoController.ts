import os = require("os");

export default class InfoController {

    public async info(): Promise<any> {
        return {
            env: process.env.NODE_ENV,
            buildDir: process.cwd(),
            hostname: os.hostname(),
        }
    }

}