import {default as PaginationBuilder, Pagination} from "../utils/PaginationBuilder";
import {default as Team} from "../models/Team";
import TeamProvider from "../providers/TeamProvider";

export default class TeamController {

    public async getTeamsList(pagination: Pagination): Promise<Team[]> {
        let count = await TeamProvider.getTeamsCount();

        if (count === 0) {
            return [];
        }

        let offset = PaginationBuilder.calculateOffser(pagination, count);

        return await TeamProvider.getTeamsChunk(offset, pagination.limit);
    }
    
}