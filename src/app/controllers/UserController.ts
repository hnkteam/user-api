import async = require("async");
import DB from "../models/DB";
import Team from "../models/Team";
import TeamProvider from "../providers/TeamProvider";
import UserImportInterface from "../entities/UserImportInterface";
import UserProvider from "../providers/UserProvider";
import {default as User, UserProperties} from "../models/User";
import {Pagination, default as PaginationBuilder} from "../utils/PaginationBuilder";

export default class UserController {

    public async importUsers(importData: UserImportInterface[]): Promise<void> {

        await DB.getInstance().getSequelize().transaction(async (transaction) => {

            let teams = await this.getOrCreateTeamObjects(importData, transaction);
            await this.updateOrCreateUsers(importData, teams, transaction);

        });
    }

    public async getUsersList(pagination: Pagination): Promise<User[]> {
        let count = await UserProvider.getUsersCount();

        if (count === 0) {
            return [];
        }

        let offset = PaginationBuilder.calculateOffser(pagination, count);

        return await UserProvider.getUsersChunk(offset, pagination.limit);
    }

    private async getOrCreateTeamObjects(
        importData: UserImportInterface[],
        transaction: any
    ): Promise<{ [teamName: string]: Team }> {
        let teamNames = [...new Set(importData.map(user => user.team))];

        let existingTeams = await TeamProvider.getTeamsIndexedByName(teamNames);
        let notExistingTeamNames = teamNames.filter(teamName => { return !existingTeams.hasOwnProperty(teamName); });

        if (notExistingTeamNames.length === 0) {
            return existingTeams;
        }

        let teamsToCreate = notExistingTeamNames.map(teamName => { return { name: teamName }; });

        await Team.bulkCreate(teamsToCreate, { transaction: transaction });
        let createdTeams = await Team.findAll({ where: { name: notExistingTeamNames }, transaction: transaction });

        for (const createdTeam of createdTeams) {
            existingTeams[createdTeam.name] = createdTeam;
        }

        return existingTeams;
    }

    private async updateOrCreateUsers(
        importData: UserImportInterface[],
        teams: { [teamName: string]: Team },
        transaction: any
    ): Promise<void> {
        let emails: string[] = importData.map(data => data.email);

        let existingUsers = await UserProvider.getUsersByEmails(emails);

        let usersToUpdate: UserProperties[] = [];
        let usersToCreate: UserProperties[] = [];
        for (const userData of importData) {
            let data = {
                firstName: userData.firstName,
                lastName: userData.lastName,
                email: userData.email,
                roleDescription: userData.roleDescription,
                teamId: teams[userData.team].id,
            };
            if (existingUsers.hasOwnProperty(userData.email)) {
                usersToUpdate.push(data);
            } else {
                usersToCreate.push(data);
            }
        }

        return new Promise<void>((resolve, reject) => {
            async.waterfall(
                [
                    (next) => {
                        if (usersToUpdate.length === 0) {
                            return next();
                        }

                        async.eachSeries(
                            usersToUpdate,
                            async (userToUpdate, eachNext) => {
                                await User.update(userToUpdate, { where: { email: userToUpdate.email }, transaction: transaction });
                                eachNext();
                            },
                            next
                        );
                    },
                    (next) => {
                        if (usersToCreate.length === 0) {
                            return next();
                        }

                        async.eachSeries(
                            usersToCreate,
                            async (userToUCreate, eachNext) => {
                                await User.create(userToUCreate, { transaction: transaction });
                                eachNext();
                            },
                            next
                        );
                    }
                ],
                (error) => {
                    if (error) {
                        reject(error);
                        return;
                    }

                    resolve();
                }
            );
        });

    }

}