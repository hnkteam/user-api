import {
    Table,
    Column,
    Model,
    PrimaryKey,
    AutoIncrement,
    ForeignKey, BelongsTo, Length, IsEmail, AllowNull
} from 'sequelize-typescript';
import Team from "./Team";

export interface UserProperties {
    id?: number;
    email: string;
    firstName: string;
    lastName: string;
    roleDescription: string;
    createdAt?: Date;
    updatedAt?: Date;
}

@Table({
    tableName: "users",
})
export default class User extends Model<User> implements UserProperties {

    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @IsEmail
    @Length({ max: 255 })
    @Column
    email: string;

    @Length({ max: 255 })
    @AllowNull
    @Column({
        field: "first_name",
    })
    firstName: string;

    @Length({ max: 255 })
    @AllowNull
    @Column({
        field: "last_name",
    })
    lastName: string;

    @AllowNull
    @Column({
        field: "role_description",
    })
    roleDescription: string;

    @Column({
        field: "created_at",
    })
    createdAt: Date;

    @Column({
        field: "updated_at",
    })
    updatedAt: Date;

    @ForeignKey(() => Team)
    @Column({
        field: "team_id",
    })
    teamId: number;

    @BelongsTo(() => Team)
    team: Team;

}