import {Model, Sequelize} from "sequelize-typescript";
import {SequelizeConfig} from "sequelize-typescript/lib/types/SequelizeConfig";

export interface DBOptions {
    connection: SequelizeConfig;
}

export default class DB {
    
    private static instance: DB = null;

    private options: DBOptions;

    private models: Array<typeof Model>;

    private sequelize: Sequelize;

    public static init(options: DBOptions, models: Array<typeof Model>): void {
        if (this.instance) {
            throw new Error('DB has already been initialized');
        }

        this.instance = new DB(options, models);
    }

    public static getInstance(): DB {
        if (!this.instance) {
            throw new Error('DB must be initialized');
        }

        return this.instance;
    }


    private constructor(options: DBOptions, models: Array<typeof Model>) {
        this.options = options;
        this.models = models;

        this.sequelize = new Sequelize(options.connection);
        this.sequelize.addModels(models);
    }

    public getSequelize(): Sequelize {
        return this.sequelize;
    }

}

