import config = require("config");
import DB from "./DB";
import LoggerFactory from "../utils/LoggerFactory";
import Team from "./Team";
import User from "./User";

export default class DBFactory {

    private static initialized: boolean = false;

    public static initializeDB(): void {
        if (this.initialized) {
            return;
        }

        let dbConfig: any = config.get('db');

        let logging = (dbConfig.logger) ? // ...
            (message?: any, ...optionalParams: any[]) => { LoggerFactory.getInstance().getLogger("app").debug(message, optionalParams); } :
            false;

        DB.init(
            {
                connection: {
                    username: dbConfig.username,
                    password: dbConfig.password,
                    host: dbConfig.host,
                    database: dbConfig.database,
                    port: dbConfig.port,
                    dialect: dbConfig.dialect,
                    define: {
                        paranoid: false,
                        timestamps: false,
                        underscored: true
                    },
                    logging: logging,
                }
            },
            [
                Team,
                User,
            ]
        );

    }

}