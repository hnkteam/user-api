import {Table, Column, Model, HasMany, PrimaryKey, AutoIncrement} from 'sequelize-typescript';
import User from "./User";

export interface TeamProperties {
    id: number;
    name: string;
    createdAt: Date;
    updatedAt: Date;
}


@Table({
    tableName: "teams",
})
export default class Team extends Model<Team> implements TeamProperties {

    @PrimaryKey
    @AutoIncrement
    @Column
    id: number;

    @Column
    name: string;

    @Column({
        field: "created_at",
    })
    createdAt: Date;

    @Column({
        field: "updated_at",
    })
    updatedAt: Date;

    @HasMany(() => User)
    users: User[];

}