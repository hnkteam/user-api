import ValidationError from "./ValidationError";

export default class UserValidationError extends ValidationError {

    public static readonly USER_IMPORT_VALIDATION_ERROR: number = 20400;
    public static readonly USER_IMPORT_DUPLICATED_USER_EMAIL: number = 20401;
    public static readonly USER_IMPORT_INVALID_USER_EMAIL: number = 20402;
    public static readonly USER_IMPORT_EMPTY_USER_EMAIL: number = 20403;
    public static readonly USER_IMPORT_EMPTY_TEAM: number = 20404;

    constructor(code: number) {
        super(code);
    }
}