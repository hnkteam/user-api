import ValidationError from "./ValidationError";

export default class PaginationValidationError extends ValidationError {

    public static readonly PAGINATION_VALIDATION_ERROR: number = 20500;
    public static readonly INVALID_PAGE_TYPE: number = 20501;
    public static readonly INVALID_PAGE: number = 20502;
    public static readonly INVALID_LIMIT_TYPE: number = 20503;
    public static readonly INVALID_LIMIT: number = 20504;
    public static readonly LIMIT_EXCEEDS_MAX_LIMIT: number = 20505;

}