import ApiError from "./ApiError";

export default class ServerError extends ApiError {

    public static readonly KERNEL_RUN_ERROR: number = 5001;
    public static readonly KERNEL_SERVER_NOT_INITIALIZED: number = 5002;

}