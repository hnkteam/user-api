import httpStatus = require("http-status");
import ApiError from "./ApiError";

export default class BadRequestError extends ApiError {

    public static readonly MISSING_PARAMETER = 4001;
    public static readonly MISSING_INPUT_FILE = 4002;
    public static readonly FILE_PARSING_ERROR = 4003;

    constructor(code: number) {
        super(code);
        this.httpStatus = httpStatus.BAD_REQUEST;
    }
}