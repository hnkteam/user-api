import httpStatus = require("http-status");

export default class ApiError extends Error {

    public static readonly INTERNAL_ERROR: number = 500;

    protected code: number;

    protected subErrors: ApiError[];

    protected previous: Error;

    protected httpStatus: number;

    protected additionalData: { [property: string]: any };

    constructor(code: number);
    constructor(code: number, message: string);
    constructor() {
        let code = ApiError.INTERNAL_ERROR;
        let message;

        switch (arguments.length) {
            case 2:
                code = arguments[0];
                message = arguments[1];
                break;
            case 1:
                code = arguments[0];
                break;
        }

        super(message);
        this.code = code;
        this.subErrors = [];
        this.httpStatus = httpStatus.INTERNAL_SERVER_ERROR;
        this.additionalData = null;
    }

    public static createFromUnknownError(error: Error, code?: number): ApiError {
        if (error instanceof ApiError) {
            return error;
        }

        const apiError = new ApiError(code || ApiError.INTERNAL_ERROR, error.message);

        apiError.setPrevious(error);

        return apiError;
    }

    public getCode(): number {
        return this.code;
    }

    public getMessage(): string {
        return this.message;
    }

    public getPrevious(): Error {
        return this.previous;
    }

    public getHttpStatus(): number {
        return this.httpStatus;
    }

    public setHttpStatus(httpStatus: number) {
        this.httpStatus = httpStatus;
    }

    public getSubSerrors(): ApiError[] {
        return this.subErrors;
    }

    public setSubErrors(subErrors: ApiError[]): ApiError {
        this.subErrors = subErrors;
        return this;
    }

    public addSubError(subError: ApiError): ApiError {
        this.subErrors.push(subError);
        return this;
    }

    public hasSubErrors(): boolean {
        return this.subErrors.length > 0;
    }

    public setAdditionalData(additionalData: { [property: string]: any }): void {
        this.additionalData = additionalData;
    }

    public getAdditionalData(): { [property: string]: any } {
        return this.additionalData;
    }

    public hasAdditionalData(): boolean {
        return this.additionalData !== null;
    }

    private setPrevious(error: Error): void {
        this.previous = error;
    }

}