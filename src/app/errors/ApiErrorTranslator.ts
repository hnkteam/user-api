import ApiError from "./ApiError";
import UserValidationError from "./UserValidationError";
import BadRequestError from "./BadRequestError";
import PaginationValidationError from "./PaginationValidationError";

export default class ApiErrorTranslator {

    public static getErrorMessage(error: ApiError): string {
        if (error.getMessage()) {
            return error.getMessage();
        }

        switch (error.getCode()) {
            case UserValidationError.USER_IMPORT_VALIDATION_ERROR:
                return "Input validation error";
            case UserValidationError.USER_IMPORT_DUPLICATED_USER_EMAIL:
                return "Duplicated user email";
            case UserValidationError.USER_IMPORT_INVALID_USER_EMAIL:
                return "User email is invalid";
            case UserValidationError.USER_IMPORT_EMPTY_USER_EMAIL:
                return "User email is empty";
            case UserValidationError.USER_IMPORT_EMPTY_TEAM:
                return "User team is empty";
            case BadRequestError.MISSING_PARAMETER:
                return "Parameter is missing";
            case BadRequestError.MISSING_INPUT_FILE:
                return "Input file is required";
            case PaginationValidationError.PAGINATION_VALIDATION_ERROR:
                return "Pagination validation error";
            case PaginationValidationError.INVALID_PAGE_TYPE:
                return "Invalid page parameter type";
            case PaginationValidationError.INVALID_PAGE:
                return "Invalid page parameter";
            case PaginationValidationError.INVALID_LIMIT_TYPE:
                return "Invalid limit parameter type";
            case PaginationValidationError.INVALID_LIMIT:
                return "Invalid limit parameter";
            case PaginationValidationError.LIMIT_EXCEEDS_MAX_LIMIT:
                return "Requested limit exceeds max limit";
            default:
                return "Internal error";
        }
    }

}