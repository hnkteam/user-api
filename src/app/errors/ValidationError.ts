import httpStatus = require("http-status");
import ApiError from "./ApiError";

export default class ValidationError extends ApiError {

    constructor(code: number) {
        super(code);
        this.httpStatus = httpStatus.BAD_REQUEST;
    }

}