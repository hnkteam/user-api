import * as Stream from "stream";
import iconv = require("iconv-lite");
import csvParser = require('csv-parse');
import BadRequestError from "../errors/BadRequestError";

export interface CsvUploadHandlerOptions {
    inputEncoding: string;
    outputEncoding: string;
    parserOptions?: csvParser.Options;
}

export default class CsvUploadHandler<T> {

    private readonly parser: csvParser.Parser;

    private readonly data: T[];

    private options: CsvUploadHandlerOptions;

    private finished: boolean;

    constructor(options: CsvUploadHandlerOptions) {
        this.options = options;
        this.parser = csvParser(options.parserOptions);
        this.data = [];
        this.finished = false;
    }

    public readStream(stream: Stream): void {

        this.parser.on('readable', () =>{
            let record;
            while(record = this.parser.read()){
                this.data.push(record);
            }
        });

        this.parser.on("finish", () => {
            this.finished = true;
        });

        this.parser.on("error", (error) => {
            throw BadRequestError.createFromUnknownError(error, BadRequestError.FILE_PARSING_ERROR);
        });

        stream
            .pipe(iconv.decodeStream(this.options.inputEncoding))
            .pipe(iconv.encodeStream(this.options.outputEncoding))
            .pipe(this.parser);
    }

    public getParser(): csvParser.Parser {
        return this.parser;
    }

    public getData(): T[] {
        return this.data;
    }

    public isFinished(): boolean {
        return this.finished;
    }
}
