
interface LoggerInterface {
    debug(obj: Object, ...params: any[]): void;
    info(error: Object, ...params: any[]): void;
    warn(obj: Object, ...params: any[]): void;
    error(obj: Object, ...params: any[]): void;
}

export default LoggerInterface;