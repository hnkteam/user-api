import restify = require("restify");
import ApiError from "../errors/ApiError";
import ApiErrorResponseHelper from "./ApiErrorResponseHelper";
import LoggerFactory from "./LoggerFactory";

export default class RestifyHelper {

    public static sendErrorResponse(
        response: restify.Response,
        error: ApiError,
        next: restify.Next|null = null
    ):void {
        LoggerFactory.getInstance().getLogger("app").error(error);

        let apiError = ApiError.createFromUnknownError(error);

        return this.sendResponse(
            response,
            apiError.getHttpStatus(),
            {
                error: ApiErrorResponseHelper.transformForResponse(apiError)
            },
            next
        );
    }

    public static sendResponse(
        response: restify.Response,
        status: number,
        data: any,
        next: restify.Next|null
    ):void {
        if (!response.finished) {
            response.charSet('utf-8');
            response.send(status, data);
        }

        if (next !== null) {
            next();
        }
    }

    public static logRequest(request: restify.Request): void {
        console.log('params', request.params);
        console.log('files', request.files);
        console.log('body', request.body);
        console.log('headers', request.headers);
    }
}