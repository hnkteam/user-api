import validator = require("validator");
import ObjectUtils from "./ObjectUtils";
import PaginationValidationError from "../errors/PaginationValidationError";

export interface Pagination {
    page: number;
    limit: number;
}

export default class PaginationBuilder {

    constructor(private defaultPage: number, private defaultLimit: number, private maxLimit: number) {}

    public static calculateOffser(pagination: Pagination, count: number): number {
        let offset = (pagination.page - 1) * pagination.limit;

        if (count <= offset) {
            pagination.page = Math.ceil(count / pagination.limit);
            offset = (pagination.page - 1) * pagination.limit;
        }

        return offset;
    }

    public build(requestedPage: any, requestedLimit: any): Pagination {
        let page = this.defaultPage;
        let limit = this.defaultLimit;

        if (!ObjectUtils.isNullOrUndefined(requestedPage)) {
            this.validateRequestedPage(requestedPage);
            page = parseInt(requestedPage);
        }

        if (!ObjectUtils.isNullOrUndefined(requestedLimit)) {
            this.validateRequestedLimit(requestedLimit);
            limit = parseInt(requestedLimit);
        }

        return {
            page: page,
            limit: limit,
        };
    }

    private validateRequestedPage(requestedPage: any): void {
        if (!validator.isInt(requestedPage)) {
            throw new PaginationValidationError(PaginationValidationError.INVALID_PAGE_TYPE);
        }

        if (parseInt(requestedPage) < 1) {
            throw new PaginationValidationError(PaginationValidationError.INVALID_PAGE);
        }
    }

    private validateRequestedLimit(requestedLimit: any): void {
        if (!validator.isInt(requestedLimit)) {
            throw new PaginationValidationError(PaginationValidationError.INVALID_LIMIT_TYPE);
        }

        let requestedLimitInt = parseInt(requestedLimit);
        if (requestedLimitInt < 1) {
            throw new PaginationValidationError(PaginationValidationError.INVALID_LIMIT);
        }

        if (requestedLimitInt > this.maxLimit) {
            throw new PaginationValidationError(PaginationValidationError.LIMIT_EXCEEDS_MAX_LIMIT);
        }
    }
}