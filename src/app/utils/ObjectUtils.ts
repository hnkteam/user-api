
export default class ObjectUtils {

    public static isNullOrUndefined(value: any): boolean {
        return typeof value === "undefined" || value === null;
    }

}