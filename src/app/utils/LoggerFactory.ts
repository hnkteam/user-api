import fs = require("fs");
import path = require("path");
import Logger = require("bunyan");
import {LoggerOptions, Stream} from "bunyan";

export interface LoggerFactoryOptions {
    baseDir: string;
    loggerOptions: { [name: string]: LoggerOptions }
}

export default class LoggerFactory {

    private static instance: LoggerFactory = null;

    private options: LoggerFactoryOptions;

    private loggers: { [name: string]: Logger } | {} = {};

    public static init(options: LoggerFactoryOptions): void {
        if (this.instance) {
            throw new Error('LoggerFactory has already been initialized');
        }

        this.instance = new LoggerFactory(options);
    }

    public static getInstance(): LoggerFactory {
        if (!this.instance) {
            throw new Error('LoggerFactory must be initialized');
        }

        return this.instance;
    }

    private constructor(options: LoggerFactoryOptions) {
        this.options = options;
    }

    public getLogger(name: string): Logger {
        this.initializeLoggerIfNeeded(name);

        return this.loggers[name];
    }

    private initializeLoggerIfNeeded(name: string) {
        if (this.loggers[name]) {
            return;
        }

        if (!this.options.loggerOptions[name]) {
            throw new Error(`Logger configuration for ${name} does not exist`);
        }

        let loggerOptions = this.options.loggerOptions[name];

        this.loggers[name] = Logger.createLogger({
            name: name,
            streams: [this.getStreamForLogger(loggerOptions)]
        });

    }


    private getStreamForLogger(loggerOptions: LoggerOptions): Stream {
        let logFilePath = path.resolve(path.join(this.options.baseDir, loggerOptions.path)).replace('{env}', process.env.NODE_ENV);
        this.prepareLogFile(logFilePath);

        return {
            type: 'file',
            level: loggerOptions.level,
            path: logFilePath,
        };
    }

    private prepareLogFile(logFilePath: string): void {
        if (fs.existsSync(logFilePath)) {
            return;
        }

        fs.closeSync(fs.openSync(logFilePath, 'w'));
    }

}