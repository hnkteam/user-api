import ApiError from "../errors/ApiError";
import ApiErrorTranslator from "../errors/ApiErrorTranslator";

export interface ApiErrorResponseInterface {
    code: number;
    message: string;
    errors?: ApiErrorResponseHelper[];
    additionalData?: { [property: string]: any };
    trace?: any
}

export default class ApiErrorResponseHelper {

    public static transformForResponse(error: ApiError): ApiErrorResponseInterface {
        let result: ApiErrorResponseInterface = {
            code: error.getCode(),
            message: ApiErrorTranslator.getErrorMessage(error),
            // trace: error.stack // @TODO - remove outside dev env
        };

        if (error.hasAdditionalData()) {
            result.additionalData = error.getAdditionalData();
        }

        if (error.hasSubErrors()) {
            result.errors = error.getSubSerrors().map((subError) => { return ApiErrorResponseHelper.transformForResponse(subError); })
        }

        return result;
    }

}