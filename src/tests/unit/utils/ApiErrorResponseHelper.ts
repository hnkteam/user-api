import assert = require("assert");
import ApiError from "../../../app/errors/ApiError";
import ApiErrorResponseHelper from "../../../app/utils/ApiErrorResponseHelper";

describe("ApiErrorResponseHelper", function () {

    describe("transformForResponse", function () {

        it("should return code and message if subErrors not set", function () {
            let code = 10001;
            let message = "test error";

            let apiError = new ApiError(code, message);

            let result = ApiErrorResponseHelper.transformForResponse(apiError);

            assert.deepEqual(
                result,
                {
                    code: code,
                    message: message,
                }
            );
        });

        it("should return code, message and subErrors", function () {
            let mainErrorCode = 10001;
            let mainErrorMessage = "test error";
            let subError1Code = 10002;
            let subError1Message = "test subError 1";
            let subError2Code = 10003;
            let subError2Message = "test subError 2";

            let apiError = new ApiError(mainErrorCode, mainErrorMessage);
            apiError.setSubErrors([
                new ApiError(subError1Code, subError1Message),
                new ApiError(subError2Code, subError2Message),
            ]);

            let result = ApiErrorResponseHelper.transformForResponse(apiError);

            assert.deepEqual(
                result,
                {
                    code: mainErrorCode,
                    message: mainErrorMessage,
                    errors: [
                        {
                            code: subError1Code,
                            message: subError1Message,
                        },
                        {
                            code: subError2Code,
                            message: subError2Message,
                        },
                    ]
                }
            );
        });

    });

});