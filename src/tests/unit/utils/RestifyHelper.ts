import assert = require("assert");
import config = require("config");
import path = require("path");
import {ObjectMock} from "../../mocks/ObjectMock";
import RestifyHelper from "../../../app/utils/RestifyHelper";
import SinonTestHelper from "../../helpers/SinonTestHelper";
import ApiError from "../../../app/errors/ApiError";
import {ClassMock} from "../../mocks/ClassMock";
import LoggerFactory from "../../../app/utils/LoggerFactory";

class RestifyResponseMock {
    finished: boolean;
    charSet(): void {}
    send(): void {}
}

describe("RestifyHelper", function () {

    const restifyResponseMock = new ObjectMock(RestifyResponseMock);
    const RestifyHelperMock = new ClassMock(RestifyHelper);

    before(() => {
        LoggerFactory.init({
            loggerOptions: config.get('logger'),
            baseDir: path.resolve(path.join(__dirname, '../../../../'))
        }); // @todo - mock
    });

    describe("#sendResponse", function () {

        it("should set charset and call send if response is not finished", function (done) {
            let response = new RestifyResponseMock();
            response.finished = false;
            let data = {
                val: 1,
            };
            let httpStatus = 201;

            const charSetSpy = restifyResponseMock.getInstanceMethodSpy("charSet");
            const sendSpy = restifyResponseMock.getInstanceMethodSpy("send");

            RestifyHelper.sendResponse(
                <any>response,
                httpStatus,
                data,
                <any>function (error) {
                    assert.equal(error, null);

                    SinonTestHelper.testSpyCall(
                        charSetSpy,
                        {
                            "0": "utf-8",
                        }
                    );
                    SinonTestHelper.testSpyCall(
                        sendSpy,
                        {
                            "0": httpStatus,
                            "1": data,
                        }
                    );

                    done();
                }
            );
        });

        it("should do nothing if response has already been finished", function (done) {
            let response = new RestifyResponseMock();
            response.finished = true;
            let data = {
                val: 1,
            };
            let httpStatus = 201;

            const charSetSpy = restifyResponseMock.getInstanceMethodSpy("charSet");
            const sendSpy = restifyResponseMock.getInstanceMethodSpy("send");

            RestifyHelper.sendResponse(
                <any>response,
                httpStatus,
                data,
                <any>function (error) {
                    assert.equal(error, null);

                    assert(charSetSpy.notCalled, "charSet should not be called");
                    assert(charSetSpy.notCalled, "send should not be called");

                    done();
                }
            );
        });

    });

    describe("#sendErrorResponse", function () {

        it("should transform error and call sendResponse", function (done) {
            let httpStatus = 503;
            let errorCode = 100001;
            let errorMessage = "test error";
            let apiError = new ApiError(errorCode, errorMessage);
            apiError.setHttpStatus(httpStatus);
            let response = new RestifyResponseMock();
            response.finished = false;

            const sendSpy = RestifyHelperMock.getStaticMethodSpy("sendResponse");

            RestifyHelper.sendErrorResponse(
                <any>response,
                apiError,
                <any> function (error) {
                    assert.equal(error, null);

                    SinonTestHelper.testSpyCall(
                        sendSpy,
                        {
                            "0": response,
                            "1": httpStatus,
                            "2": {
                                error: {
                                    code: errorCode,
                                    message: errorMessage,
                                }
                            }
                        }
                    );

                    done();
                }
            );

        });

    });

});