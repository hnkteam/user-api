import assert = require("assert");
import ObjectUtils from "../../../app/utils/ObjectUtils";

describe("ObjectUtils", function () {

    describe("#isNullOrUndefined", function () {

        it("should return true if argument is undefined", function () {
            let value = undefined;
            assert.equal(ObjectUtils.isNullOrUndefined(value), true);
        });

        it("should return true if argument is null", function () {
            let value = null;
            assert.equal(ObjectUtils.isNullOrUndefined(value), true);
        });

        it("should return true if argument is defined", function () {
            let value = 1;
            assert.equal(ObjectUtils.isNullOrUndefined(value), false);
        });

    });

});