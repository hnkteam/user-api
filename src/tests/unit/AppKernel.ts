import assert = require("assert");
import DummyLogger from "../mocks/DummyLogger";
import AppKernel from "../../app/AppKernel";

describe("AppKernel", function () {

    describe("#run", function () {

        it("should initialize restify server and start listening", function (done) {
            let logger = new DummyLogger();
            let name = "dummy-app";
            let host = "localhost";
            let port = 1234;

            const appKernel = new AppKernel(logger);

            appKernel.run({ name: name, log: logger }, [port, host])
                .then(() => {
                    assert.ok(appKernel.getServer(), "server should be set");
                    assert.equal(appKernel.getServer().name, name);

                    done();
                })
                .catch((error) => {
                    console.log(error);
                    assert(false, "this test should not reach this assert");
                    done(error);
                });

        });

    });

});