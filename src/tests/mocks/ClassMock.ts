import sinon = require('sinon');

export class ClassMock {

    protected objectToMock: any;
    protected sandbox: any;
    protected classMock: any;

    constructor(objectToMock: any, autoRegister:boolean = true) {
        this.objectToMock = objectToMock;
        if (autoRegister) {
            this.register();
        }
    }

    public register(): void {
        const self = this;

        beforeEach(function () {
            self.doBeforeEach();
        });

        afterEach(function () {
            self.doAfterEach();
        });
    }

    public getStaticMethodSpy(method: string): sinon.SinonSpy {
        return this.sandbox.spy(this.classMock.object, method);
    }

    public getStaticMethodStub(method: string): sinon.SinonStub {
        return this.sandbox.stub(this.classMock.object, method);
    }

    public doBeforeEach(): void {
        this.initializeSandbox();
        this.initializeClassMock();
    }

    public doAfterEach(): void {
        this.verifyClassMock();
        this.restoreSandbox();
    }

    protected initializeSandbox(): void {
        this.sandbox = sinon.sandbox.create();
    }

    protected initializeClassMock(): void {
        this.classMock = this.sandbox.mock(this.objectToMock);
    }

    protected restoreSandbox(): void {
        this.sandbox.restore();
    }

    protected verifyClassMock(): void {
        this.classMock.verify();
    }

}
