import sinon = require("sinon");
import {ClassMock} from './ClassMock';

export class ObjectMock extends ClassMock {

    protected instanceMock: any;

    public doBeforeEach(): void {
        super.doBeforeEach();
        this.instanceMock = this.sandbox.mock(this.objectToMock.prototype);
    }

    public doAfterEach(): void {
        this.instanceMock.verify();
        super.doAfterEach();
    }

    public getInstanceMethodSpy(method: string): sinon.SinonSpy {
        return this.sandbox.spy(this.instanceMock.object, method);
    }

    public getInstanceMethodStub(method): sinon.SinonStub {
        return this.sandbox.stub(this.instanceMock.object, method);
    }
}
