import async = require("async");
import User from "../../app/models/User";
import Team from "../../app/models/Team";

export default class FixtureManager {

    public static fixtures: any;

    public static getFixtures(): any {
        return this.fixtures;
    }

    public static async setupFixtures(): Promise<any> {
        await this.resetDatabase();
        await this.insertFixtures();
    }

    private static async resetDatabase(): Promise<any> {
        this.fixtures  = {
            team: [],
            user: [],
        };
        await User.truncate({
            cascade: true,
            force: true,
        });
        await Team.truncate({
            cascade: true,
            force: true,
        });
    }

    private static insertFixtures(): Promise<void> {
        let fixtures = this.getFixtureTemplates();

        return new Promise<void>((resolve, reject) => {
            async.waterfall(
                [
                    (next) => {
                        async.eachSeries(
                            fixtures.team,
                            (fixture, eachNext) => {
                                Team.create(fixture)
                                    .catch(eachNext)
                                    .then(team => {
                                        this.fixtures.team.push(team);
                                        eachNext();
                                    });
                            },
                            next
                        )
                    },
                    (next) => {
                        async.eachSeries(
                            fixtures.user,
                            (fixture: any, eachNext) => {
                                fixture.teamId = this.fixtures.team[fixture.teamId].id;
                                User.create(fixture)
                                    .catch(eachNext)
                                    .then(user => {
                                        this.fixtures.user.push(user);
                                        eachNext();
                                    });
                            },
                            next
                        )
                    }
                ],
                (error) => {
                    if (error) {
                        reject(error);
                        return;
                    }

                    resolve();
                }
            );
        });

    }

    private static getFixtureTemplates(): any {
        return {
            team: [
                {
                    name: "Developers",
                },
                {
                    name: "Admins"
                },
            ],
            user: [
                {
                    firstName: "Jan",
                    lastName: "Kowalski",
                    email: "jan.kowalski@example.com",
                    roleDescription: "nic nie robi",
                    teamId: 0
                },
                {
                    firstName: "Adam",
                    lastName: "Nowak",
                    email: "adam.nowak@example.com",
                    roleDescription: null,
                    teamId: 0
                },
            ]
        };
    }
}

