import assert = require("assert");
import supertest = require("supertest");
import FixtureManager from "../../fixtures/FixtureManager";
import server = require("./../../../main");

describe("UserController", function () {

    describe("GET /user", function () {

        context("valid request", function () {

            beforeEach((done) => {
                FixtureManager.setupFixtures()
                    .catch(done)
                    .then(done);
            });

            it("should return paginated list of users with teams", function (done) {

                let expectedUsers = {};
                expectedUsers[ FixtureManager.getFixtures().user[1].id] =  FixtureManager.getFixtures().user[1];

                supertest(server.server)
                    .get("/user?page=2&limit=1")
                    .send(null)
                    .end((error, response: supertest.Response) => {
                        assert(Array.isArray(response.body), "response should be an array");
                        assert.equal(response.body.length, Object.keys(expectedUsers).length, `unexpected number of users ${response.body.length}`);

                        for (const user of response.body) {
                            assert(expectedUsers.hasOwnProperty(user.id), `unexpected user ${user.id}`);
                            assert.ok(user.team, `expected team in user response`);
                        }

                        done();
                    });
            });

        });

    });

});