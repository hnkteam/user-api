import assert = require("assert");
import async = require("async");
import path = require("path");
import supertest = require("supertest");
import FixtureManager from "../../fixtures/FixtureManager";
import ResponseTestHelper from "../../helpers/ResponseTestHelper";
import server = require("./../../../main");
import Team from "../../../app/models/Team";
import User from "../../../app/models/User";

describe('UserController', function () {

    describe("PUT /user", function () {

        context("invalid request", function () {

            it("should return error if no file was sent in request", function (done) {

                supertest(server.server)
                    .put("/user")
                    .send(null)
                    .end((error, response: supertest.Response) => {

                        ResponseTestHelper.assertErrorResponse(
                            response,
                            400,
                            4002,
                            "Input file is required"
                        );

                        done();
                    });

            });

            it("should validate file", function (done) {

                supertest(server.server)
                    .put("/user?encoding=utf8")
                    .send(null)
                    .attach("importFile.csv", path.resolve("src/tests/fixtures/invalid_import_1.csv"))
                    .end((error, response: supertest.Response) => {
                        ResponseTestHelper.assertErrorResponse(
                            response,
                            400,
                            20400,
                            "Input validation error",
                            [
                                {
                                    code: 20403,
                                    message: "User email is empty",
                                    additionalData: { item: 1 }
                                },
                                {
                                    code: 20402,
                                    message: "User email is invalid",
                                    additionalData: { item: 2 }
                                },

                            ]
                        );

                        done();
                    });

            });

        });


    });

    context("valid request", function () {

        beforeEach((done) => {
            FixtureManager.setupFixtures()
                .catch(done)
                .then(done);
        });

        it("should create new users and teams, and update existing users", function (done) {

            let expectedTeams = [
                "Developers",
                "Admins",
                "PM"
            ];
            let expectedUsers = {};
            expectedUsers["jan.kowalski@example.com"] = {
                firstName: "Jan",
                lastName: "Nowakowski",
                roleDescription: "po zmianie nazwiska zaczal cos robic",
                team: "Developers",
                shouldBeUpdated: true,
            };
            expectedUsers["ola@example.com"] = {
                firstName: "Ola",
                lastName: null,
                roleDescription: "DBA",
                team: "Admins",
                shouldBeUpdated: false,
            };
            expectedUsers["szef@example.com"] = {
                firstName: null,
                lastName: null,
                roleDescription: null,
                team: "PM",
                shouldBeUpdated: false,
            };

            supertest(server.server)
                .put("/user?encoding=utf8")
                .send(null)
                .attach("importFile.csv", path.resolve("src/tests/fixtures/valid_import_1.csv"))
                .end((error, response: supertest.Response) => {
                    assert.equal(response.status, 204);

                    async.waterfall(
                        [
                            function checkTeams(next) {
                                Team.findAll()
                                    .catch(next)
                                    .then((teams) => {
                                        assert.equal(teams.length, expectedTeams.length, `unexpected number of teams ${teams.length}`);

                                        let teamsIndexedByName = {};
                                        for (const team of teams) {
                                            assert.notEqual(expectedTeams.indexOf(team.name), -1, `unexpected team ${team.name}`);
                                            teamsIndexedByName[team.name] = team;
                                        }

                                        next(null, teamsIndexedByName);
                                    });
                            },

                            function checkUsers(teamsIndexedByName, next) {
                                User.findAll({ where: { email: Object.keys(expectedUsers) } })
                                    .catch(next)
                                    .then((users) => {
                                        assert.equal(users.length, expectedTeams.length, `unexpected number of users ${users.length}`);

                                        for (const user of users) {
                                            assert(expectedUsers.hasOwnProperty(user.email), `unexpected user ${user.email}`);
                                            let expectedUser = expectedUsers[user.email];
                                            assert.equal(user.firstName, expectedUser.firstName);
                                            assert.equal(user.lastName, expectedUser.lastName);
                                            assert.equal(user.roleDescription, expectedUser.roleDescription);

                                            if (expectedUser.shouldBeUpdated) {
                                                assert.notEqual(user.updatedAt, null);
                                            } else {
                                                assert.equal(user.updatedAt, null);
                                            }

                                            assert.equal(user.teamId, teamsIndexedByName[expectedUser.team]);
                                        }

                                        next();
                                    });
                            }
                        ]
                    );

                    done();
                });

        });

    });

});