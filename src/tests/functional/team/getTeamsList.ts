import assert = require("assert");
import supertest = require("supertest");
import FixtureManager from "../../fixtures/FixtureManager";
import server = require("./../../../main");

describe("TeamController", function () {

    describe("GET /team", function () {

        context("valid request", function () {

            beforeEach((done) => {
                FixtureManager.setupFixtures()
                    .catch(done)
                    .then(done);
            });

            it("should return paginated list of users with teams", function (done) {

                let expectedTeams = {};
                expectedTeams[ FixtureManager.getFixtures().team[1].id] =  FixtureManager.getFixtures().team[1];

                supertest(server.server)
                    .get("/team?page=2&limit=1")
                    .send(null)
                    .end((error, response: supertest.Response) => {
                        assert(Array.isArray(response.body), "response should be an array");
                        assert.equal(response.body.length, Object.keys(expectedTeams).length, `unexpected number of teams ${response.body.length}`);

                        for (const team of response.body) {
                            assert(expectedTeams.hasOwnProperty(team.id), `unexpected team ${team.id}`);
                            assert.ok(team.users, `expected users in team response`);
                        }

                        done();
                    });
            });

        });

    });

});