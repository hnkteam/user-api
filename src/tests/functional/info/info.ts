import path = require("path");
import supertest = require("supertest");
import assert = require("assert");
import server = require("./../../../main");

describe("InfoController", function () {

    before(function (done) {
        if (null === server.server.address()) {
            server.server.on('listening', done);
        } else {
            done();
        }
    });

    describe("GET /info", function () {

        context("valid request", function () {

            it("should return OK and system info", function (done) {

                supertest(server.server)
                    .get("/info")
                    .send(null)
                    .end((error, response: supertest.Response) => {
                        assert.equal(error, null);
                        assert.equal(response.status, 200);
                        assert.deepEqual(
                            response.body,
                            {
                                env: process.env.NODE_ENV,
                                buildDir: path.resolve(path.join(__dirname, '../../../../')),
                                hostname: "user-api",
                            }
                        );

                        done();
                    });

            })

        });

    });

});