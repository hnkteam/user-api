import assert = require("assert");
import supertest = require("supertest");

export interface ApiErrorInterface {
    code: number;
    message?: string;
    subErrors?: ApiErrorInterface[];
    additionalData?: { [key: string]: any; }
}

export default class ResponseTestHelper {

    public static assertErrorResponse(
        response: supertest.Response,
        expectedHttpStatus: number,
        expectedErrorCode: number,
        expectedMessage?: string,
        expectedSubErrors?: ApiErrorInterface[],
        expectedAdditionalData?: { [key: string]: any; }
    ): void {
        assert.equal(response.status, expectedHttpStatus, `unexpected http status: ${response.status}`);
        assert.ok(response.body.error, "error property was expected in response");

        this.assertSameApiError(
            response.body.error,
            {
                code: expectedErrorCode,
                message: expectedMessage,
                subErrors: expectedSubErrors,
                additionalData: expectedAdditionalData,
            }
        );
    }

    public static assertSameApiError(
        error: any,
        expectedError: ApiErrorInterface
    ): void {
        assert.equal(error.code, expectedError.code, `unexpected error code: ${error.code}`);

        if (expectedError.message) {
            assert.equal(error.message, expectedError.message, `unexpected error message: ${error.expectedMessage}`);
        }

        if (expectedError.additionalData) {
            assert.deepEqual(error.additionalData, expectedError.additionalData);
        }

        if (expectedError.subErrors) {
            assert.ok(error.errors, 'error should contain errors property');
            assert(Array.isArray(error.errors), 'errors property should be an array');
            assert.equal(error.errors.length, expectedError.subErrors.length, `invalid number of sub errors ${error.errors.length}`);

            for (const subError of expectedError.subErrors) {
                this.assertErrorContainsSubError(error, subError);
            }
        }
    }

    public static assertErrorContainsSubError(
        error: any,
        expectedSubError: ApiErrorInterface
    ): void {
        let found = true;

        for (const subError of error.errors) {
            try {
                this.assertSameApiError(subError, expectedSubError);
                found = true;
                break;
            } catch (error) {
                continue;
            }
        }

        assert.equal(found, true, `sub error not found ${expectedSubError.code}`);
    }
}