import assert = require("assert");
import sinon = require("sinon");
import ObjectUtils from "../../app/utils/ObjectUtils";

export default class SinonTestHelper {

    public static logSpyCalls(spy: sinon.SinonSpy, id: string = ""): void {
        if (spy.notCalled) {
            console.log(`${id}spy was not called`);
            return;
        }

        console.log(`${id}call count: ${spy.callCount}`);
        for (let i = 0; i < spy.callCount; i++) {
            let call = spy.getCall(i);
            console.log(`${id}call ${i}`, call.args);
        }
    }

    public static testSpyCall(
        spy: sinon.SinonSpy,
        expectedArgs: { [index: number]: any } = null,
        callCount: number = 1,
        testCall: number = 0,
        id: string = ""
    ): void {
        assert.equal(spy.callCount, callCount, `${id}spy should be called ${callCount} times, called ${spy.callCount}`);

        if (null === expectedArgs) {
            return;
        }

        let call = spy.getCall(testCall);
        assert.equal(ObjectUtils.isNullOrUndefined(testCall), false, `${id}spy call ${testCall} does not exist`);

        let args = call.args;
        for (let index of Object.keys(expectedArgs)) {
            if (false === expectedArgs.hasOwnProperty(index)) {
                continue;
            }
            assert.notEqual(typeof args[index], 'undefined', `${id}spy argument on index ${index} does not exist, expected ${expectedArgs[index]}`);

            assert.deepEqual(args[index], expectedArgs[index]);
        }
    }

}