import meow = require("meow");
import os = require("os");
import path = require("path");
import AppKernel from "./app/AppKernel";
import LoggerFactory from "./app/utils/LoggerFactory";

const cli = meow({
    help: [
        'Arguments:',
        '--env=ENVIRONMENT_NAME',
        '   development, test',
    ].join(os.EOL)
});

process.env.NODE_ENV = (cli.flags.hasOwnProperty("env")) ? cli.flags['env'] :  process.env.NODE_ENV || 'development';
process.env.NODE_CONFIG_DIR = path.resolve(path.join(__dirname, '../config'));
process.env.TZ = 'Europe/Warsaw';

const config = require('config');

LoggerFactory.init({
    baseDir: path.resolve(path.join(__dirname, '../')),
    loggerOptions: config.get("logger"),
});

const app = new AppKernel(LoggerFactory.getInstance().getLogger("app"));

app.run({
    name: 'user-api'
}, [8080]);

export const server = app.getServer();

process.on('exit', function() {});

process.on('SIGINT', function() {
    if (process.env.NODE_ENV === 'test') {
        process.exit();
    }

    console.log('Received SIGINT signal');
    process.exit();
});

process.on('uncaughtException', function(err) {
    if (process.env.NODE_ENV === 'test') {
        return;
    }

    console.log('Uncaught exception', err);
});

process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
});